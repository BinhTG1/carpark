package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.example.carpark.models.ParkingLot;

public interface ParkingLotManagementService {
	public Page<ParkingLot> parkingLotList(Pageable pageable);

	public ParkingLot addParkingLot(ParkingLot parkingLot);

	public Optional<ParkingLot> updateParkingLot(ParkingLot parkingLot);

	public Boolean deleteParkingLot(Long id);

	public Slice<ParkingLot> searchParkLot(String name, Pageable pageable);
}
