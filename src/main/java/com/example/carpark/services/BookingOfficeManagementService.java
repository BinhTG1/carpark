package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.example.carpark.models.BookingOffice;
import com.example.carpark.models.BookingOfficeView;

public interface BookingOfficeManagementService {
	public Page<BookingOfficeView> bookingOfficeList(Pageable pageable);

	public BookingOffice addBookingOffice(BookingOffice bookingOffice);

	public Slice<BookingOfficeView> searchOffice(String name, Pageable page);
}
