package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.example.carpark.models.BookingOffice;
import com.example.carpark.models.BookingOfficeView;
import com.example.carpark.repositories.BookingOfficeRepository;

@Service
public class BookingOfficeManagementServiceImpl implements BookingOfficeManagementService {
	@Autowired
	private BookingOfficeRepository bookingOfficeRepository;

	@Override
	public Page<BookingOfficeView> bookingOfficeList(Pageable pageable) {
		// TODO Auto-generated method stub
		return bookingOfficeRepository.findAllBy(pageable);
	}

	@Override
	public BookingOffice addBookingOffice(BookingOffice bookingOffice) {
		// TODO Auto-generated method stub
		return bookingOfficeRepository.save(bookingOffice);
	}

	@Override
	public Slice<BookingOfficeView> searchOffice(String name, Pageable page) {
		// TODO Auto-generated method stub
		return bookingOfficeRepository.findAllByOfficeNameStartingWithIgnoreCase(name, page);
	}

}
