package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.example.carpark.models.ParkingLot;
import com.example.carpark.models.Trip;
import com.example.carpark.models.TripView;
import com.example.carpark.repositories.TripRepository;

@Service
public class TripManagementServiceImpl implements TripManagementService {
	@Autowired
	private TripRepository tripRepository;

	@Override
	public Page<TripView> tripList(Pageable pageable) {
		// TODO Auto-generated method stub
		return tripRepository.findAllBy(pageable);
	}

	@Override
	public Trip addTrip(Trip trip) {
		// TODO Auto-generated method stub
		return tripRepository.save(trip);
	}

	@Override
	public Optional<Trip> findById(Long id) {
		// TODO Auto-generated method stub
		return tripRepository.findById(id);
	}

	@Override
	public Optional<Trip> updateTrip(Trip trip) {
		// TODO Auto-generated method stub
		Optional<Trip> t = tripRepository.findById(trip.getTripId());
		if (t.isPresent()) {
			return t.map(tr -> {
				tr.setBookedTicketNumber(trip.getBookedTicketNumber());
				tr.setCarType(trip.getCarType());
				tr.setDepartureDate(trip.getDepartureDate());
				tr.setDepartureTime(trip.getDepartureTime());
				tr.setDestination(trip.getDestination());
				tr.setDriver(trip.getDriver());
				tr.setMaximumOnlineTicketNumber(trip.getMaximumOnlineTicketNumber());

				return tripRepository.save(tr);
			});
		}
		return null;
	}

	@Override
	public Boolean deleteTrip(Long id) {
		Optional<Trip> pl = tripRepository.findById(id);
		if (pl.isPresent()) {
			tripRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public Slice<TripView> searchTrip(String name, Pageable pageable) {
		// TODO Auto-generated method stub
		return tripRepository.findByDriverStartingWithIgnoreCase(name, pageable);
	}

}
