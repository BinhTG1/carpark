package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.example.carpark.models.Ticket;
import com.example.carpark.models.TicketView;

public interface TicketManagementService {
	public Page<TicketView> ticketList(Pageable pageable);

	public Ticket addTicket(Ticket ticket);

	public Boolean deleteTicket(Long id);

	public Slice<TicketView> searchTicket(String name, Pageable pageable);
}
