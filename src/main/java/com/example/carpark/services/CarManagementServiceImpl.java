package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.example.carpark.models.Car;
import com.example.carpark.models.Trip;
import com.example.carpark.repositories.CarRepository;

@Service
public class CarManagementServiceImpl implements CarManagementService {

	@Autowired
	private CarRepository carRepository;

	@Override
	public Page<Car> carList(Pageable pageable) {
		// TODO Auto-generated method stub
		return carRepository.findAllBy(pageable);
	}

	@Override
	public Car addCar(Car car) {
		// TODO Auto-generated method stub
		return carRepository.save(car);
	}

	@Override
	public Optional<Car> updateCar(Car car) {
		// TODO Auto-generated method stub
		Optional<Car> c = carRepository.findByLicensePlate(car.getLicensePlate());

		if (c.isPresent()) {
			return c.map(ca -> {
				ca.setCarColor(car.getCarColor());
				ca.setCarType(car.getCarType());
				ca.setCompany(car.getCompany());
				ca.setParkingLot(car.getParkingLot());
				return carRepository.save(ca);
			});
		}
		return null;
	}

	@Override
	public Boolean deleteCar(String licensePlate) {
		// TODO Auto-generated method stud
		Optional<Car> ca = carRepository.findByLicensePlate(licensePlate);
		if (ca.isPresent()) {
			carRepository.deleteById(licensePlate);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Slice<Car> searchCar(String licensePlate, Pageable pageable) {
		// TODO Auto-generated method stub
		return carRepository.findByLicensePlateStartingWithIgnoreCase(licensePlate, pageable);
	}

}
