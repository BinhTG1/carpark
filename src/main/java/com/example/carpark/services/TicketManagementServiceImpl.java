package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.example.carpark.models.Car;
import com.example.carpark.models.Ticket;
import com.example.carpark.models.TicketView;
import com.example.carpark.repositories.TicketRepository;

@Service
public class TicketManagementServiceImpl implements TicketManagementService {

	@Autowired
	private TicketRepository ticketRepository;

	@Override
	public Page<TicketView> ticketList(Pageable pageable) {
		// TODO Auto-generated method stub
		return ticketRepository.findAllBy(pageable);
	}

	@Override
	public Ticket addTicket(Ticket ticket) {
		// TODO Auto-generated method stub
		return ticketRepository.save(ticket);
	}

	@Override
	public Boolean deleteTicket(Long id) {
		// TODO Auto-generated method stub
		Optional<Ticket> ti = ticketRepository.findByTicketId(id);
		if (ti.isPresent()) {
			ticketRepository.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Slice<TicketView> searchTicket(String name, Pageable pageable) {
		// TODO Auto-generated method stub
		return ticketRepository.findByCustomerNameStartingWithIgnoreCase(name, pageable);
	}

}