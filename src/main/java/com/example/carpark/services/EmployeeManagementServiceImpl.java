package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.example.carpark.models.Employee;
import com.example.carpark.models.EmployeeView;
import com.example.carpark.repositories.EmployeeRepository;

@Service
public class EmployeeManagementServiceImpl implements EmployeeManagementService {
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employee addEmployee(Employee employee) {
//		// TODO Auto-generated method stub
		return employeeRepository.save(employee);
	}

	@Override
	public Optional<List<EmployeeView>> displayEmployeeList() {
		// TODO Auto-generated method stub
		return employeeRepository.findAllEmployeeBy();
	}

	@Override
	public Optional<EmployeeView> duplicatedAccount(String account) {
		// TODO Auto-generated method stub
		return employeeRepository.findByAccount(account);
	}

	@Override
	public Optional<Employee> editEmployee(Employee employee, Long id) {
		// TODO Auto-generated method stub
		Optional<Employee> em = employeeRepository.findById(id);
		if (em.isPresent()) {
			return em.map(e -> {
				e.setAccount(employee.getAccount());
				e.setDepartment(employee.getDepartment());
				e.setEmployeeAddress(employee.getEmployeeAddress());
				e.setEmployeeBirthdate(employee.getEmployeeBirthdate());
				e.setEmployeeEmail(employee.getEmployeeEmail());
				e.setEmployeeName(employee.getEmployeeName());
				e.setEmployeePhone(employee.getEmployeePhone());
				e.setPassword(employee.getPassword());
				e.setSex(employee.getSex());
				return employeeRepository.save(e);
			});
		} else {
			return null;
		}
	}

	@Override
	public void deleteEmployee(Long id) {
		// TODO Auto-generated method stub
		employeeRepository.deleteById(id);
	}

	@Override
	public Page<EmployeeView> pageEmployee(Pageable pageable) {
		// TODO Auto-generated method stub
		return employeeRepository.findPageBy(pageable);
	}

	@Override
	public Slice<EmployeeView> searchEmployee(String name, Pageable pageable) {
		// TODO Auto-generated method stub
		return employeeRepository.findSliceByEmployeeNameStartingWithIgnoreCase(name, pageable);
	}

}
