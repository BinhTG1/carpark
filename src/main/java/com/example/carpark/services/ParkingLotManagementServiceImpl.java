package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import com.example.carpark.models.ParkingLot;
import com.example.carpark.repositories.ParkingLotRepository;

@Service
public class ParkingLotManagementServiceImpl implements ParkingLotManagementService {
	@Autowired
	private ParkingLotRepository parkingLotRepository;

	@Override
	public Page<ParkingLot> parkingLotList(Pageable pageable) {
		// TODO Auto-generated method stub
		return parkingLotRepository.findAllBy(pageable);
	}

	@Override
	public ParkingLot addParkingLot(ParkingLot parkingLot) {
		// TODO Auto-generated method stub
		return parkingLotRepository.save(parkingLot);
	}

	public Optional<ParkingLot> updateParkingLot(ParkingLot parkingLot) {
		Optional<ParkingLot> pl = parkingLotRepository.findById(parkingLot.getParkId());
		if (pl.isPresent()) {
			return pl.map(p -> {
				p.setParkArea(parkingLot.getParkArea());
				p.setParkName(parkingLot.getParkName());
				p.setParkPlace(parkingLot.getParkPlace());
				p.setParkPrice(parkingLot.getParkPrice());
				p.setParkStatus(parkingLot.getParkStatus());
				return parkingLotRepository.save(p);
			});
		}
		return null;
	}

	@Override
	public Boolean deleteParkingLot(Long id) {
		// TODO Auto-generated method stub
		Optional<ParkingLot> pl = parkingLotRepository.findById(id);
		if (pl.isPresent()) {
			parkingLotRepository.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Slice<ParkingLot> searchParkLot(String name, Pageable pageable) {
		// TODO Auto-generated method stub
		return parkingLotRepository.findByParkNameStartingWithIgnoreCase(name, pageable);
	}

}
