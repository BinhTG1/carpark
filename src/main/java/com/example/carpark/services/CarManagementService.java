package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.example.carpark.models.Car;

public interface CarManagementService {
	public Page<Car> carList(Pageable pageable);

	public Car addCar(Car car);

	public Optional<Car> updateCar(Car car);

	public Boolean deleteCar(String id);

	public Slice<Car> searchCar(String licensePlate, Pageable pageable);
}
