package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.example.carpark.models.Employee;
import com.example.carpark.models.EmployeeView;

public interface EmployeeManagementService {
	public Employee addEmployee(Employee employee);

	public Optional<Employee> editEmployee(Employee employee, Long id);

	public void deleteEmployee(Long id);

	public Optional<List<EmployeeView>> displayEmployeeList();

	public Optional<EmployeeView> duplicatedAccount(String account);

	public Page<EmployeeView> pageEmployee(Pageable pageable);

	public Slice<EmployeeView> searchEmployee(String name, Pageable pageable);
}
