package com.example.carpark.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.example.carpark.models.Trip;
import com.example.carpark.models.TripView;

public interface TripManagementService {
	public Page<TripView> tripList(Pageable pageable);

	public Trip addTrip(Trip trip);

	public Optional<Trip> findById(Long id);

	public Optional<Trip> updateTrip(Trip trip);

	public Boolean deleteTrip(Long id);

	public Slice<TripView> searchTrip(String name, Pageable pageable);
}
