package com.example.carpark.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Trip {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "trip_id", columnDefinition = "BIGINT(20)")
	private Long tripId;

	@Column(name = "booked_ticket_number", columnDefinition = "INT(11)")
	private Integer bookedTicketNumber;

	@Column(name = "car_type", columnDefinition = "VARCHAR(11)")
	private String carType;

	@Column(name = "departure_date", columnDefinition = "DATE")
	private String departureDate;

	@Column(name = "departure_time", columnDefinition = "TIME")
	private String departureTime;

	@Column(name = "destination", columnDefinition = "VARCHAR(50)")
	private String destination;

	@Column(name = "driver", columnDefinition = "VARCHAR(11)")
	private String driver;

	@Column(name = "maximum_online_ticket_number", columnDefinition = "INT(11)")
	private Integer maximumOnlineTicketNumber;

	Trip() {

	}
}
