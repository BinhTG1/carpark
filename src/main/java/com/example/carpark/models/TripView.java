package com.example.carpark.models;

public interface TripView {
	Long getTripId();

	String getDestination();

	String getDepartureTime();

	String getDriver();

	String getCarType();

	String getBookedTicketNumber();
}
