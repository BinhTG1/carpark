package com.example.carpark.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ticket_id", columnDefinition = "BIGINT(20)")
	private Long ticketId;

	@Column(name = "booking_time", columnDefinition = "TIME")
	private String bookingTime;

	@Column(name = "customer_name", columnDefinition = "VARCHAR(11)")
	private String customerName;

	@ManyToOne
	@JoinColumn(name = "license_plate", referencedColumnName = "license_plate")
	private Car car;

	@ManyToOne
	@JoinColumn(name = "trip_id", referencedColumnName = "trip_id")
	private Trip trip;
}
