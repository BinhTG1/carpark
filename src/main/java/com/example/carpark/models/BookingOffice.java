package com.example.carpark.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class BookingOffice {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "office_id", columnDefinition = "BIGINT(20)")
	private Long officeId;

	@Column(name = "end_contract_deadline", columnDefinition = "DATE")
	private String endContractDeadline;

	@Column(name = "office_name", columnDefinition = "VARCHAR(50)")
	private String officeName;

	@Column(name = "office_phone", columnDefinition = "VARCHAR(11)")
	private String officePhone;

	@Column(name = "office_place", columnDefinition = "VARCHAR(50)")
	private String officePlace;

	@Column(name = "office_price", columnDefinition = "BIGINT(50)")
	private Long officePrice;

	@Column(name = "start_contract_deadline", columnDefinition = "DATE")
	private String startContractDeadline;

	@ManyToOne
	@JoinColumn(name = "trip_id", referencedColumnName = "trip_id")
	private Trip trip;

	BookingOffice() {

	}
}
