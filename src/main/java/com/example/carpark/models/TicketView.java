package com.example.carpark.models;

public interface TicketView {
	Long getTicketId();

	TripDestination getTrip();

	CarLicensePlate getCar();

	String getCustomerName();

	String getBookingTime();

	interface TripDestination {
		String getDestination();
	}

	interface CarLicensePlate {
		String getLicensePlate();
	}
}
