package com.example.carpark.models;

public interface BookingOfficeView {
	Long getOfficeId();

	String getOfficeName();

	TripDestination getTrip();

	interface TripDestination {
		String getDestination();
	}
}
