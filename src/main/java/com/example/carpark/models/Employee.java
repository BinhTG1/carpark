package com.example.carpark.models;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(columnDefinition = "BIGINT(20)")
	private Long employeeId;

	@Column(columnDefinition = "VARCHAR(50) NOT NULL UNIQUE")
	private String account;

	@Column(columnDefinition = "VARCHAR(10) NOT NULL")
	private String department;

	@Column(columnDefinition = "VARCHAR(50)")
	private String employeeAddress;

	@Column(columnDefinition = "DATE NOT NULL")
	private String employeeBirthdate;

	@Column(columnDefinition = "VARCHAR(50)")
	private String employeeEmail;

	@Column(columnDefinition = "VARCHAR(50) NOT NULL")
	private String employeeName;

	@Column(columnDefinition = "VARCHAR(10) NOT NULL")
	private String employeePhone;

	@Column(columnDefinition = "VARCHAR(20) NOT NULL")
	private String password;

	@Column(columnDefinition = "VARCHAR(1) NOT NULL")
	private String sex;

	Employee() {

	}
}
