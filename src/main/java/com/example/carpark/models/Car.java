package com.example.carpark.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Car {
	@Id
	@Column(name = "license_plate", columnDefinition = "VARCHAR(50)")
	private String licensePlate;

	@Column(name = "car_color", columnDefinition = "VARCHAR(11)")
	private String carColor;

	@Column(name = "car_type", columnDefinition = "VARCHAR(50)")
	private String carType;

	@Column(name = "company", columnDefinition = "VARCHAR(50)")
	private String company;

	@ManyToOne
	@JoinColumn(name = "park_id", referencedColumnName = "park_id")
	private ParkingLot parkingLot;
}
