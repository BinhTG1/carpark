package com.example.carpark.models;

public interface EmployeeView {
	Long getEmployeeId();

	String getEmployeeName();

	String getEmployeeBirthdate();

	String getEmployeeAddress();

	String getEmployeePhone();

	String getDepartment();
}
