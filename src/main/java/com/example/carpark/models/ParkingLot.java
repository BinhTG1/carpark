package com.example.carpark.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ParkingLot {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "park_id", columnDefinition = "BIGINT(20)")
	private Long parkId;

	@Column(name = "park_area", columnDefinition = "BIGINT(20)")
	private Long parkArea;

	@Column(name = "park_name", columnDefinition = "VARCHAR(50)")
	private String parkName;

	@Column(name = "park_place", columnDefinition = "VARCHAR(11)")
	private String parkPlace;

	@Column(name = "park_price", columnDefinition = "BIGINT(20)")
	private Long parkPrice;

	@Column(name = "park_status", columnDefinition = "VARCHAR(50)")
	private String parkStatus;
}
