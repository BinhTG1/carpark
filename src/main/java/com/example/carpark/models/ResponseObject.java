package com.example.carpark.models;

public class ResponseObject {
	private String status;
	private String message;
	private Object obj;

	public ResponseObject(String status, String message, Object obj) {
		setStatus(status);
		setMessage(message);
		setObj(obj);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}
