package com.example.carpark.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.carpark.models.ParkingLot;
import com.example.carpark.models.ResponseObject;

@Controller
@RequestMapping(path = "/cpa/parkinglot")
public class ParkingLotController extends CarParkOperationAdministrationController {
	@GetMapping(path = "/list")
	ResponseEntity<ResponseObject> parkingLotList(@RequestParam int pagesize, @RequestParam int pagenumber) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Slice<ParkingLot> list = parkingLotService.parkingLotList(pageable);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Danh sách các bãi đỗ xe", list.get()));
	}

	@GetMapping(path = "/list/search")
	ResponseEntity<ResponseObject> searchParkingLotList(@RequestParam int pagesize, @RequestParam int pagenumber,
			@RequestParam String name) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Slice<ParkingLot> list = parkingLotService.searchParkLot(name, pageable);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Danh sách các bãi đỗ xe", list));

	}

	@PostMapping(path = "/add")
	ResponseEntity<ResponseObject> addParkingLot(@RequestBody ParkingLot parkingLot) {
		ResponseEntity<ResponseObject> re = validateParkingLot(parkingLot);
		if (re == null) {
			return ResponseEntity.status(HttpStatus.OK).body(
					new ResponseObject("OK", "Thêm bãi đỗ xe thành công", parkingLotService.addParkingLot(parkingLot)));
		} else {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Thêm thất bại", ""));
		}
		return re;
	}

	@PutMapping(path = "/update")
	ResponseEntity<ResponseObject> updateParkingLot(@RequestBody ParkingLot parkingLot) {
		ResponseEntity<ResponseObject> re = validateParkingLot(parkingLot);
		if (re == null) {
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Sửa bãi đỗ xe thành công",
					parkingLotService.updateParkingLot(parkingLot)));
		} else {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Sửa thất bại", ""));
		}
		return re;
	}

	@DeleteMapping(path = "/delete/{id}")
	ResponseEntity<ResponseObject> deleteParkingLot(@PathVariable Long id) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Xóa thành công", parkingLotService.deleteParkingLot(id)));
	}

	ResponseEntity<ResponseObject> validateParkingLot(ParkingLot parkingLot) {
		if (parkingLot.getParkArea() <= 0 || parkingLot.getParkArea() == null || parkingLot.getParkName().isBlank()
				|| parkingLot.getParkPlace().isBlank() || parkingLot.getParkPrice() <= 0
				|| parkingLot.getParkPrice() == null || parkingLot.getParkStatus().isBlank()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Tồn tại trường trống", parkingLot));
		}
		return null;
	}
}
