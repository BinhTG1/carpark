package com.example.carpark.controllers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.carpark.models.ResponseObject;
import com.example.carpark.models.TicketView;
import com.example.carpark.models.Trip;
import com.example.carpark.models.TripView;

@Controller
@RequestMapping(path = "/cpa")
public class TripController extends CarParkOperationAdministrationController {
	@GetMapping(path = "/trip/list")
	ResponseEntity<ResponseObject> tripList(@RequestParam int pagesize, @RequestParam int pagenumber) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Page<TripView> list = tripService.tripList(pageable);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Danh sách các chuyến đi", list.get()));
	}

	@GetMapping(path = "/trip/list/search")
	ResponseEntity<ResponseObject> tripList(@RequestParam int pagesize, @RequestParam int pagenumber,
			@RequestParam String name) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Slice<TripView> list = tripService.searchTrip(name, pageable);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Danh sách các chuyến đi", list.get()));
	}

	// Add trip to the database
	@PostMapping(path = "/trip/add")
	ResponseEntity<ResponseObject> addTrip(@RequestBody Trip trip) {
		ResponseEntity<ResponseObject> re = tripValidation(trip);
		if (re == null) {
			re = ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject("OK", "Trip được thêm thành công", tripService.addTrip(trip)));

		}
		return re;
	}

	@PutMapping(path = "/trip/update")
	ResponseEntity<ResponseObject> updateTrip(@RequestBody Trip trip) {
		ResponseEntity<ResponseObject> re = tripValidation(trip);
		if (re == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject("OK", "Sửa bãi đỗ xe thành công", tripService.updateTrip(trip)));
		} else {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Sửa thất bại", ""));
		}
		return re;
	}

	// Validate trip fields
	ResponseEntity<ResponseObject> tripValidation(Trip trip) {
		if (trip.getDepartureDate().isBlank() || trip.getDepartureTime().isBlank() || trip.getDriver().isBlank()
				|| trip.getCarType().isBlank() || trip.getMaximumOnlineTicketNumber() < 1
				|| trip.getDestination().isBlank()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Tồn tại trường trống", ""));
		}

		// Test if the date a valid date
		try {
			LocalDate date = LocalDate.parse(trip.getDepartureDate());
			trip.setDepartureDate(date.toString());
		} catch (DateTimeParseException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Ngày không hợp lệ", ""));
		}

		try {
			System.out.println(LocalTime.now());
			LocalTime time = LocalTime.parse(trip.getDepartureTime());
			trip.setDepartureTime(time.toString());
		} catch (DateTimeParseException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Thời gian không hợp lệ", ""));
		}
		return null;
	}
}
