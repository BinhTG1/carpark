package com.example.carpark.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.carpark.models.Employee;
import com.example.carpark.models.EmployeeView;
import com.example.carpark.models.ResponseObject;
import com.example.carpark.services.EmployeeManagementServiceImpl;

@Controller
@RequestMapping(path = "/hrm/employee")
public class HumanResourceManagerController {
	@Autowired
	private EmployeeManagementServiceImpl employeeService;
	private static final Pattern passwordPattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{6,}$");

	// Get all employees in the database.
	@GetMapping(path = "/list")
	ResponseEntity<ResponseObject> employeeList() {
		Optional<List<EmployeeView>> list = employeeService.displayEmployeeList();

		// Check if the list is empty.
		if (list.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Dữ liệu trống", ""));
		}

		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("Ok", "Lấy dữ liệu thành công", list.get()));
	}

	// Get all employees in the database.
	@GetMapping(path = "/list/page")
	ResponseEntity<ResponseObject> employeeListPage(@RequestParam(name = "page") int page,
			@RequestParam(name = "pageSize") int pageSize) {
		Pageable pageable = PageRequest.of(page, pageSize);
		Page<EmployeeView> list = employeeService.pageEmployee(pageable);

		// Check if the list is empty.
		if (list.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Dữ liệu trống", ""));
		}

		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("Ok", "Lấy dữ liệu thành công", list.get()));
	}

	// Get all employees in the database.
	@GetMapping(path = "/list/page/search")
	ResponseEntity<ResponseObject> searchEmployeeListPage(@RequestParam(name = "page") int page,
			@RequestParam(name = "pageSize") int pageSize, @RequestParam(name = "name") String name) {
		Pageable pageable = PageRequest.of(page, pageSize);
		Slice<EmployeeView> list = employeeService.searchEmployee(name, pageable);

		// Check if the list is empty.
		if (list.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Dữ liệu trống", ""));
		}

		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("Ok", "Lấy dữ liệu thành công", list.get()));
	}

	// Validate and add the employee to database.
	@PostMapping(path = "/add")
	ResponseEntity<ResponseObject> addEmployee(@RequestBody Employee employee) {
		// Validate employee if ok will return null
		ResponseEntity<ResponseObject> re = validateEmployee(employee);

		// Check if that employee's account already exists.
		Optional<EmployeeView> employeeView = employeeService.duplicatedAccount(employee.getAccount());
		if (employeeView.isPresent()) {
			re = ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Tài khoản nhân viên đã tồn tại", ""));
		}

		if (re == null) {
			re = ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Nhân viên đã được thêm thành công",
					employeeService.addEmployee(employee)));
		}
		// Save the employee to the database.
		return re;
	}

	// Validate the employee
	ResponseEntity<ResponseObject> validateEmployee(Employee employee) {
		// Check if there's any empty field.
		if (employee.getEmployeeName().isBlank() || employee.getEmployeePhone().isBlank()
				|| employee.getEmployeeBirthdate() == null || employee.getSex().isBlank()
				|| employee.getAccount().isBlank() || employee.getPassword().isBlank()
				|| employee.getDepartment().isBlank()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Dữ liệu đầu vào chưa hoàn thiện", ""));
		}

		// Check if the password meets the requirements.
		Matcher matcher = passwordPattern.matcher(employee.getPassword());
		if (!matcher.matches()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Mật khẩu không hợp lệ", ""));
		}

		// Test if the date a valid date
		try {
			LocalDate date = LocalDate.parse(employee.getEmployeeBirthdate());
			employee.setEmployeeBirthdate(date.toString());
			if (LocalDate.now().getYear() - date.getYear() < 16) {
				System.out.println(LocalDate.now().getYear());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseObject("Bad Request", "Tuổi nhân viên chưa đủ", ""));
			}
		} catch (DateTimeParseException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Ngày không hợp lệ", ""));
		}

		return null;
	}

	// Validate and edit the employee
	@PutMapping(path = "/edit/{id}")
	ResponseEntity<ResponseObject> editEmployee(@RequestBody Employee employee, @PathVariable Long id) {
		// Validate employee if ok will return null
		ResponseEntity<ResponseObject> re = validateEmployee(employee);
		if (re == null) {
			// Check if the employee can be edit or not
			Optional<EmployeeView> employeeView = employeeService.duplicatedAccount(employee.getAccount());
			// Only editable when the same account want to update to itself.
			if (employeeView.get().getEmployeeId() == id) {
				Optional<Employee> e = employeeService.editEmployee(employee, id);
				if (e.isPresent()) {
					re = ResponseEntity.status(HttpStatus.OK)
							.body(new ResponseObject("OK", "Nhân viên đã được sửa thành công", e));
				}
			} else {
				re = ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseObject("Bad Request", "Sửa không thành công", ""));
			}

		}

		return re;
	}

	// Delete employee by id
	@DeleteMapping(path = "/delete/{id}")
	ResponseEntity<ResponseObject> deleteEmployee(@PathVariable Long id) {
		employeeService.deleteEmployee(id);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Nhân viên đã được xóa thành công", ""));
	}
}
