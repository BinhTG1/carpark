package com.example.carpark.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.carpark.models.Car;

import com.example.carpark.models.ResponseObject;

@Controller
@RequestMapping(path = "/cpa/car")
public class CarController extends CarParkOperationAdministrationController {
	@GetMapping(path = "/list")
	ResponseEntity<ResponseObject> carList(@RequestParam int pagesize, @RequestParam int pagenumber) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Page<Car> list = carService.carList(pageable);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Danh sách  xe", list.get()));
	}

	@GetMapping(path = "/list/search")
	ResponseEntity<ResponseObject> SearchCarList(@RequestParam int pagesize, @RequestParam int pagenumber,
			@RequestParam String name) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Slice<Car> list = carService.searchCar(name, pageable);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Danh sách  xe", list.get()));

	}

	@PostMapping(path = "/add")
	ResponseEntity<ResponseObject> addCar(@RequestBody Car car) {
		ResponseEntity<ResponseObject> re = validateCar(car);
		if (re == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject("OK", "Thêm xe thành công", carService.addCar(car)));
		} else {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Thêm thất bại", ""));
		}
		return re;
	}

	@PutMapping(path = "/update")
	ResponseEntity<ResponseObject> updateCar(@RequestBody Car car) {
		ResponseEntity<ResponseObject> re = validateCar(car);
		if (re == null) {
			Optional<Car> c = carService.updateCar(car);
			if (c.isPresent()) {
				return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Sửa xe thành công", c));
			}
		} else {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Sửa thất bại", ""));
		}
		return re;
	}

	@DeleteMapping(path = "/delete")
	ResponseEntity<ResponseObject> deleteParkingLot(@RequestParam(name = "licensePlate") String licensePlate) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Xóa thành công", carService.deleteCar(licensePlate)));
	}

	private ResponseEntity<ResponseObject> validateCar(Car car) {
		if (car.getCarColor().isBlank() || car.getCarType().isBlank() || car.getCompany().isBlank()
				|| car.getLicensePlate().isBlank() || car.getParkingLot() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Tồn tại trường trống", car));
		}
		return null;
	}
}
