package com.example.carpark.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.example.carpark.services.BookingOfficeManagementServiceImpl;
import com.example.carpark.services.CarManagementServiceImpl;
import com.example.carpark.services.ParkingLotManagementServiceImpl;
import com.example.carpark.services.TicketManagementServiceImpl;
import com.example.carpark.services.TripManagementServiceImpl;

public class CarParkOperationAdministrationController {
	@Autowired
	protected BookingOfficeManagementServiceImpl bookingOfficeService;

	@Autowired
	protected TripManagementServiceImpl tripService;

	@Autowired
	protected ParkingLotManagementServiceImpl parkingLotService;

	@Autowired
	protected CarManagementServiceImpl carService;

	@Autowired
	protected TicketManagementServiceImpl ticketService;

}
