package com.example.carpark.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.carpark.models.Car;
import com.example.carpark.models.ResponseObject;
import com.example.carpark.models.Ticket;
import com.example.carpark.models.TicketView;

@Controller
@RequestMapping(path = "/cpa/ticket")
public class TicketController extends CarParkOperationAdministrationController {
	@GetMapping(path = "/list")
	ResponseEntity<ResponseObject> ticketList(@RequestParam int pagesize, @RequestParam int pagenumber) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Page<TicketView> list = ticketService.ticketList(pageable);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Danh sách vé", list.get()));
	}

	@GetMapping(path = "/list/search")
	ResponseEntity<ResponseObject> searchTicketList(@RequestParam int pagesize, @RequestParam int pagenumber,
			@RequestParam String name) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Slice<TicketView> list = ticketService.searchTicket(name, pageable);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Danh sách vé", list.get()));
	}

	@PostMapping(path = "/add")
	ResponseEntity<ResponseObject> addTicket(@RequestBody Ticket ticket) {
		ResponseEntity<ResponseObject> re = validateTicket(ticket);
		if (re == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject("OK", "Thêm vé thành công", ticketService.addTicket(ticket)));
		} else {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Thêm thất bại", ""));
		}
		return re;
	}

	@DeleteMapping(path = "/delete/{id}")
	ResponseEntity<ResponseObject> deleteParkingLot(@PathVariable Long id) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Xóa thành công", ticketService.deleteTicket(id)));
	}

	private ResponseEntity<ResponseObject> validateTicket(Ticket ticket) {
		if (ticket.getBookingTime().isBlank() || ticket.getCar() == null || ticket.getCustomerName().isBlank()
				|| ticket.getTrip() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Tồn tại trường trống", ticket));
		}
		return null;
	}
}
