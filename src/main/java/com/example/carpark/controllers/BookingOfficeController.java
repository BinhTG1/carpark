package com.example.carpark.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.carpark.models.BookingOffice;
import com.example.carpark.models.BookingOfficeView;
import com.example.carpark.models.ResponseObject;
import com.example.carpark.models.Trip;

@Controller
@RequestMapping(path = "/cpa/bookingoffice")
public class BookingOfficeController extends CarParkOperationAdministrationController {
	@GetMapping(path = "/list")
	ResponseEntity<ResponseObject> bookingOfficeList(@RequestParam int pagesize, @RequestParam int pagenumber) {
		Pageable pageable = PageRequest.of(pagenumber, pagesize);
		Page<BookingOfficeView> bookingOfficeView = bookingOfficeService.bookingOfficeList(pageable);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Danh sách văn phòng", bookingOfficeView.get()));

	}

	@GetMapping(path = "/list/search")
	ResponseEntity<ResponseObject> searchBookingOfficeList(@RequestParam int pagesize, @RequestParam int pagenumber,
			@RequestParam String name) {
		Pageable pageable = PageRequest.of(pagesize, pagenumber);
		Slice<BookingOfficeView> bookingOfficeView = bookingOfficeService.searchOffice(name, pageable);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseObject("OK", "Danh sách văn phòng", bookingOfficeView.get()));
	}

	@PostMapping(path = "/add")
	ResponseEntity<ResponseObject> addBookingOffice(@RequestBody BookingOffice bookingOffice) {
		ResponseEntity<ResponseObject> re = bookingOfficeValidation(bookingOffice);
		// Check if trip exist or not
		Optional<Trip> tripView = tripService.findById(bookingOffice.getTrip().getTripId());
		if (tripView.isEmpty()) {
			re = ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body(new ResponseObject("No Content", "Chuyến đi này chưa tồn tại", ""));
		} else {
			re = ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("OK", "Văn phòng được thêm thành công",
					bookingOfficeService.addBookingOffice(bookingOffice)));
		}
		return re;
	}

	ResponseEntity<ResponseObject> bookingOfficeValidation(BookingOffice bookingOffice) {
		if (bookingOffice.getOfficeName().isBlank() || bookingOffice.getTrip() == null
				|| bookingOffice.getOfficePhone().isBlank() || bookingOffice.getOfficePlace().isBlank()
				|| bookingOffice.getOfficePrice() < 1 || bookingOffice.getStartContractDeadline().isBlank()
				|| bookingOffice.getEndContractDeadline().isBlank()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Tồn tại trường trống", ""));
		}

		try {
			LocalDate end = LocalDate.parse(bookingOffice.getEndContractDeadline());
			LocalDate start = LocalDate.parse(bookingOffice.getStartContractDeadline());
			if (end.isBefore(start)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseObject("Bad Request", "Thời gian ngày hợp lệ", ""));
			}
		} catch (DateTimeParseException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseObject("Bad Request", "Thời gian ngày hợp lệ", ""));
		}
		return null;
	}
}
