package com.example.carpark.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.carpark.models.Employee;
import com.example.carpark.models.EmployeeView;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	Optional<List<EmployeeView>> findAllEmployeeBy();

	Optional<EmployeeView> findByAccount(String account);

	Page<EmployeeView> findPageBy(Pageable pageable);

//	@Query(value = "Select  ")
	Slice<EmployeeView> findSliceByEmployeeNameStartingWithIgnoreCase(String employeeName, Pageable pageable);
}
