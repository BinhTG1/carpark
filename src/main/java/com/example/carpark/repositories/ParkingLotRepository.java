package com.example.carpark.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.carpark.models.ParkingLot;

public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long> {
	Page<ParkingLot> findAllBy(Pageable pageable);

	Optional<ParkingLot> findById(Long id);

	Slice<ParkingLot> findByParkNameStartingWithIgnoreCase(String parkName, Pageable page);

}
