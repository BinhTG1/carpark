package com.example.carpark.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.carpark.models.Car;
import com.example.carpark.models.Ticket;
import com.example.carpark.models.TicketView;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
	Page<TicketView> findAllBy(Pageable pageable);

	Optional<Ticket> findByTicketId(Long ticketId);

	Slice<TicketView> findByCustomerNameStartingWithIgnoreCase(String customerName, Pageable pageable);
}
