package com.example.carpark.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.carpark.models.Trip;
import com.example.carpark.models.TripView;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {
	Page<TripView> findAllBy(Pageable pageable);

	Slice<TripView> findByDriverStartingWithIgnoreCase(String name, Pageable pageable);
}
