package com.example.carpark.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.carpark.models.BookingOffice;
import com.example.carpark.models.BookingOfficeView;

@Repository
public interface BookingOfficeRepository extends JpaRepository<BookingOffice, Long> {
	Page<BookingOfficeView> findAllBy(Pageable pageable);

	Slice<BookingOfficeView> findAllByOfficeNameStartingWithIgnoreCase(String officeName, Pageable pageable);
}
